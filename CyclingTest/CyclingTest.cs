﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using cycling;

namespace CyclingTest
{
    [TestClass]
    public class CyclingTest
    {   
        /// Test the Max Process
        [TestMethod]
        public void HeartRate()
        {
            //arrange
            int HR = 130;

            int count = 1;

            Cycling test = new Cycling();

            //act
            test.MaxHR = 100;

            test.HeartRate(HR, count);

            //assert
            Assert.AreEqual(130,test.MaxHR);
            
        }

        /// Test the Min Process
        [TestMethod]
        public void SpeedMin()
        {
            //arrange
            int SP = 130;

            int count = 1;

            Cycling test = new Cycling();

            //act
            test.MinSP = 100;

            test.Speed(SP, count);

            //assert
            Assert.AreEqual(100, test.MinSP);

        }

        ///Test the Aveage Process when count = 1
        [TestMethod]
        public void SpeedAver1()
        {
            Cycling test = new Cycling();

            //arrange when count =1 

            int SP = 13;

            int count = 1;

            //act
            test.AverSP = 0.0;

            test.Speed(SP, count);

            //assert
            Assert.AreEqual(13.0, test.AverSP);

        }

        ///Test the Aveage Process when count >= 2
        [TestMethod]
        public void SpeedAver2()
        {
            Cycling test = new Cycling();

            //arrange when count =1 

            int SP1 = 13;

            int count1 = 1;

            test.AverSP = 0.0;
            //act
            test.Speed(SP1, count1);

            //arrange when count =1 

            int SP2 = 14;

            int count2 = 2;

            //act
            test.Speed(SP2, count2);

            //assert
            Assert.AreEqual(13.5, test.AverSP);

        }

        ///Test the Distance Process
        [TestMethod]
        public void Distance()
        {
            Cycling test = new Cycling();

            //arrange when count =1 

            int SP = 3600;

            int count = 1;

            //act
            test.Distance = 0;

            test.Speed(SP, count);

            //assert
            Assert.AreEqual(1, test.Distance);

        }

        ///Test the Km to mile
        [TestMethod]
        public void kmToMile()
        {
            Cycling test = new Cycling();

            //arrange

            int SP = 3600;

            int count = 1;

            test.KM = false;

            //act
            test.mile1 = 0;

            test.Drate = 0.62;

            test.Speed(SP, count);

            //assert
            Assert.AreEqual(1 * test.Drate, test.mile1);

        }

    }
}
