﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cycling
{
    public partial class Chart : Form
    {
        private static int _HRata = 0;

        public int HRata
        {
            get
            {
                return _HRata;
            }
            set
            {
                _HRata = value;
            }
        }

        public Chart(string path)
        {   
            InitializeComponent();
            HRData(path);
        }
        /// <summary>
        /// Read HRData from the file which loaded on the Cycling.cs
        /// </summary>
        /// <param name="path"></param>
        public void HRData(string path)
        {
            int count = 0;

            System.IO.StreamReader file = new System.IO.StreamReader(path);
            String line = file.ReadLine();

            while ((line = file.ReadLine()) != null)
            {
                String[] splitline = line.Split('\t');

                if (line == "[HRData]")
                {
                    while ((line = file.ReadLine()) != null)    //add records to every datagridview
                    {
                        String[] splitline1 = line.Split('\t');

                        HeartRata(int.Parse(splitline1[0]), count+1);

                        count++;

                        this.listBox1.Items.Add(line);
                    }
                }
            }
        }

        public void HeartRata(int everyHRata, int count)
        {
            HRata = everyHRata;

            this.chart1.Series["HRData"].Points.AddXY(count, HRata);
            
        }



    }
}
